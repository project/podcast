<?php

namespace Drupal\podcast\Plugin\views\style;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\podcast\PodcastViewsMappingsTrait;
use Drupal\views\Plugin\views\style\Rss as ViewsRss;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Default style plugin to render an RSS feed.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "podcast_rss",
 *   title = @Translation("Podcast RSS Feed"),
 *   help = @Translation("Generates a podcast RSS feed from a view."),
 *   theme = "views_view_rss_podcast_feed",
 *   display_types = {"feed"}
 * )
 */
class Rss extends ViewsRss {

  use PodcastViewsMappingsTrait;

  /**
   * The podcast elements to add to the feed.
   *
   * @var array
   */
  public array $podcastElements;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  public LanguageManagerInterface $languageManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('request_stack')
    );
  }

  /**
   * Constructs a RSS plugin object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LanguageManagerInterface $language_manager, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->languageManager = $language_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    unset($options['description']);
    $keys = [
      'copyright_field',
      'title_field',
      'description_field',
      'link_field',
      'lastBuildDate_field',
      'itunes:explicit_field',
      'itunes:owner--name_field',
      'itunes:owner--email_field',
      'itunes:author_field',
      'itunes:summary_field',
      'itunes:keywords_field',
      'itunes:image_field',
      'itunes:category_field',
      'itunes:type',
      'itunes:new-feed-url_field',
      'podcast:guid_field',
      'podcast:funding_field',
      'podcast:license_field',
      'podcast:medium_field',
      'podcast:locked_field',
      'podcast:value_type_field',
      'podcast:value_method_field',
      'podcast:value_suggested_field',
      'podcast:value_recipient_name_field',
      'podcast:value_recipient_type_field',
      'podcast:value_recipient_address_field',
      'podcast:value_recipient_split_field',
    ];
    $options = array_reduce($keys, function ($options, $key) {
      $options[$key] = ['default' => ''];
      return $options;
    }, $options);
    $options['generator'] = ['default' => 'Podcast module for Drupal'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);
    $view_fields_labels = $this->displayHandler->getFieldLabels();
    unset($form['description']);

    $form['title_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Title field'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['title_field'],
      '#description' => $this->t('Podcast name to display in the feed. Defaults to the view name.'),
      '#required' => TRUE,
    ];
    $form['description_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Description field'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['description_field'],
      '#description' => $this->t('Podcast description to display in the feed.'),
      '#required' => FALSE,
    ];
    $form['link_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Link field'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['link_field'],
      '#description' => $this->t('Podcast link to the feed.'),
      '#required' => TRUE,
    ];
    $form['lastBuildDate_field'] = [
      '#type' => 'select',
      '#title' => $this->t('lastBuildDate field'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['lastBuildDate_field'],
      '#description' => $this->t('When the feed was last build.'),
      '#required' => TRUE,
    ];
    $form['generator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Generator'),
      '#default_value' => $this->options['generator'],
      '#description' => $this->t('Enter the text you want to display on how this feed was generated.'),
      '#maxlength' => 1024,
    ];
    $form['language'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Language'),
      '#default_value' => $this->options['language'],
      '#description' => $this->t('The language spoken on the show in <a href="@url">ISO 639</a> format: two letter language codes with modifiers, for example: "fr-ca". Defaults to current language.', ['@url' => 'https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes']),
      '#maxlength' => 10,
    ];
    $form['copyright_field'] = [
      '#type' => 'select_or_other_select',
      '#title' => $this->t('Copyright'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['copyright_field'],
      '#description' => $this->t('Copyright notice for the podcast.'),
    ];
    $form['itunes:explicit_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Explicit field'),
      '#description' => $this->t('Signal iTunes weather or not this podcast is explicit. Expects a boolean.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:explicit_field'],
      '#required' => FALSE,
    ];
    $form['itunes:owner--name_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Feed Owner Name'),
      '#description' => $this->t('Owner name for the iTunes feed.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:owner--name_field'],
      '#required' => FALSE,
    ];
    $form['itunes:owner--email_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Feed Owner E-mail'),
      '#description' => $this->t('Owner email for the iTunes feed.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:owner--email_field'],
      '#required' => FALSE,
    ];
    $form['itunes:author_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Feed Author'),
      '#description' => $this->t('List of owner names names for the iTunes feed.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:author_field'],
      '#required' => FALSE,
    ];
    $form['itunes:summary_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Summary'),
      '#description' => $this->t('Summary to be displayed in iTunes.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:summary_field'],
      '#required' => FALSE,
    ];
    $form['itunes:keywords_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Keywords'),
      '#description' => $this->t('Keywords to be displayed in iTunes.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:keywords_field'],
      '#required' => FALSE,
    ];
    $form['itunes:image_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Image'),
      '#description' => $this->t('Image to be displayed in iTunes.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:image_field'],
      '#required' => FALSE,
    ];
    $form['itunes:category_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Category'),
      '#description' => $this->t('Categories to be displayed in iTunes. Processor expects "$category/$subcategory". Multivalues are coma separated.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:category_field'],
      '#required' => FALSE,
    ];
    $form['itunes:type'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Type'),
      '#description' => $this->t('The type of show. If your show is Serial you must use this tag..'),
      '#options' => [
        'episodic' => $this->t('Episodic: episodes are intended to be consumed without any specific order'),
        'serial' => $this->t('Serial: episodes are intended to be consumed in sequential order'),
      ],
      '#default_value' => !empty($this->options['itunes:type']) ? $this->options['itunes:type'] : 'episodic',
      '#required' => FALSE,
    ];
    $form['itunes:new-feed-url_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes New Feed URL field'),
      '#description' => $this->t('The URL to the new iTunes feed. This is used when moving the feed from one URL to another.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:new-feed-url_field'],
    ];
    $form['podcast:guid_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Podcast GUID'),
      '#description' => $this->t('This element is used to declare a unique, global identifier for a podcast.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:guid_field'],
    ];
    $form['podcast:funding_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Funding Link'),
      '#description' => $this->t('This tag lists possible donation/funding links for the podcast.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:funding_field'],
    ];
    $form['podcast:funding_text_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Funding Link Text'),
      '#description' => $this->t('The Text for the Funding Link.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:funding_text_field'],
    ];
    $form['podcast:license_field'] = [
      '#type' => 'select',
      '#title' => $this->t('License'),
      '#description' => $this->t('This element defines a license that is applied to the audio/video of the podcast as a whole.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:license_field'],
    ];
    $form['podcast:medium_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Medium'),
      '#description' => $this->t('The medium tag tells the an application what the content contained within the feed IS, as opposed to what the content is ABOUT in the case of a category. Should be "podcast", "music", "video", "film", "audiobook", "newsletter", or "blog".'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:medium_field'],
    ];
    $form['podcast:locked_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Locked'),
      '#description' => $this->t('This tag may be set to yes or no. The purpose is to tell other podcast platforms whether they are allowed to import this feed. A value of yes means that any attempt to import this feed into a new platform should be rejected.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:locked_field'],
    ];
    $form['podcast:payments'] = [
      '#type' => 'details',
      '#title' => $this->t('Payment Information'),
      '#description' => $this->t('This element designates the cryptocurrency or payment layer that will be used, the transport method for transacting the payments, and a suggested amount denominated in the given cryptocurrency.'),
      '#tree' => TRUE,
    ];
    $form['podcast:payments']['podcast:value_type_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Value Type'),
      '#description' => $this->t('This is the service slug of the cryptocurrency or protocol layer.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:payments']['podcast:value_type_field'],
      '#empty_option' => $this->t('- None -'),
    ];
    $form['podcast:payments']['podcast:value_method_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Value Method'),
      '#description' => $this->t('This is the transport mechanism that will be used.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:payments']['podcast:value_method_field'],
      '#empty_option' => $this->t('- None -'),
    ];
    $form['podcast:payments']['podcast:value_suggested_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Value Suggested'),
      '#description' => $this->t('This is an optional suggestion on how much cryptocurrency to send with each payment.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:payments']['podcast:value_suggested_field'],
      '#empty_option' => $this->t('- None -'),
    ];
    $form['podcast:payments']['podcast:value_recipient_name_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Recipient Name'),
      '#description' => $this->t('A free-form string that designates who or what this recipient is.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:payments']['podcast:value_recipient_name_field'],
      '#empty_option' => $this->t('- None -'),
    ];

    $form['podcast:payments']['podcast:value_recipient_type_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Recipient Type'),
      '#description' => $this->t('A slug that represents the type of receiving address that will receive the payment.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:payments']['podcast:value_recipient_type_field'],
      '#empty_option' => $this->t('- None -'),
    ];
    $form['podcast:payments']['podcast:value_recipient_address_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Recipient Address'),
      '#description' => $this->t('This denotes the receiving address of the payee.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:payments']['podcast:value_recipient_address_field'],
      '#empty_option' => $this->t('- None -'),
    ];
    $form['podcast:payments']['podcast:value_recipient_split_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Recipient Split'),
      '#description' => $this->t('The number of shares of the payment this recipient will receive.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:payments']['podcast:value_recipient_split_field'],
      '#empty_option' => $this->t('- None -'),
    ];

    // Support Select or Other fields, as they need an array for #default_value
    // when the 'other' field is used.
    foreach (Element::children($form) as $key) {
      if ($form[$key]['#type'] === 'select') {
        $form[$key]['#empty_option'] = $this->t('- None -');
      }
      if ($form[$key]['#type'] === 'select_or_other_select') {
        $form[$key]['#empty_option'] = $this->t('- None -');
        $default_value = $form[$key]['#default_value'];
        if (!isset($view_fields_labels[$default_value])) {
          $form[$key]['#other_options'] = $default_value;
          unset($form[$key]['#default_value']);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) {
    parent::validateOptionsForm($form, $form_state);
    // Flatten options for select_or_other form fields.
    foreach (Element::children($form) as $key) {
      if (isset($form[$key]['#type']) && $form[$key]['#type'] === 'select_or_other_select') {
        $values = $form_state->getValue('style_options')[$key];
        if ($values['select'] === 'select_or_other') {
          $form_state->setValueForElement($form[$key], $values['other']);
        }
        else {
          $form_state->setValueForElement($form[$key], $values['select']);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getChannelElements(): array {
    $channel_elements = parent::getChannelElements();
    $namespaces = $this->namespaces ?? [];
    $this->namespaces = array_merge($namespaces, [
      'xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd',
      'xmlns:content' => 'http://purl.org/rss/1.0/modules/content/',
      'xmlns:atom' => 'http://www.w3.org/2005/Atom',
      'xmlns:podcast' => 'https://podcastindex.org/namespace/1.0',
    ]);
    return $channel_elements;
  }

  /**
   * Return an array of additional XHTML elements to add to the podcast channel.
   *
   * @return array
   *   A key val array.
   */
  protected function getPodcastElements(): array {
    $podcast_elements = [];

    $image_field_value = $this->getField(0, $this->options['itunes:image_field']);
    $image_url = $image_field_value ? (
      UrlHelper::isExternal($image_field_value) ?
      $image_field_value :
      $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost() . $image_field_value
    ) : '';
    $parsed_image_field = parse_url($image_url);

    $podcast_elements[] = [
      'key' => 'itunes:image',
      'value' => '',
      'attributes' => ['href' => $image_url],
    ];

    $link = NULL;
    if (isset($parsed_image_field['scheme']) && $parsed_image_field['host']) {
      $link = [
        'key' => 'link',
        'value' => $parsed_image_field['scheme'] . '://' .
        $parsed_image_field['host'] .
        (
          !empty($parsed_image_field['port']) ?
          ':' . $parsed_image_field['port'] :
          ''
        ),
      ];
    }
    $podcast_elements[] = [
      'key' => 'image',
      'attributes' => [],
      'values' => [
        [
          'key' => 'url',
          'value' => $image_url,
        ],
        $link,
        [
          'key' => 'title',
          'value' => $this->getField(0, $this->options['title_field']),
        ],
      ],
    ];
    $podcast_elements[] = [
      'key' => 'generator',
      'value' => $this->options['generator'],
    ];
    $podcast_elements[] = [
      'key' => 'language',
      'value' => $this->options['language'] ?? $this->languageManager->getCurrentLanguage()->getId(),
    ];
    $podcast_elements[] = [
      'key' => 'itunes:type',
      'value' => !empty($this->options['itunes:type']) ? $this->options['itunes:type'] : 'episodic',
    ];
    $podcast_elements[] = [
      'key' => 'podcast:funding',
      'attributes' => [
        'url' => $this->getField(0, $this->options['podcast:funding_field']),
      ],
      'value' => $this->getField(0, $this->options['podcast:funding_text_field']),
    ];
    $value_type = $this->getField(0, $this->options['podcast:payments']['podcast:value_type_field']);
    if (!is_null($value_type)) {
      $podcast_elements[] = [
        'key' => 'podcast:value',
        'attributes' => [
          'type' => $value_type,
          'method' => $this->getField(0, $this->options['podcast:payments']['podcast:value_method_field']),
          'suggested' => $this->getField(0, $this->options['podcast:payments']['podcast:value_suggested_field']),
        ],
        'values' => [
          [
            'key' => 'podcast:valueRecipient',
            'attributes' => [
              'name' => $this->getField(0, $this->options['podcast:payments']['podcast:value_recipient_name_field']),
              'type' => $this->getField(0, $this->options['podcast:payments']['podcast:value_recipient_type_field']),
              'address' => $this->getField(0, $this->options['podcast:payments']['podcast:value_recipient_address_field']),
              'split' => $this->getField(0, $this->options['podcast:payments']['podcast:value_recipient_split_field']),
            ],
          ],
        ],
      ];
    }
    $keys = [
      'title',
      'description',
      'lastBuildDate',
      'copyright',
      'itunes:explicit',
      'itunes:author',
      'itunes:summary',
      'itunes:keywords',
      'podcast:guid',
      'podcast:license',
      'podcast:medium',
    ];
    // Don't map elements that have selected "other".
    $view_fields_labels = $this->displayHandler->getFieldLabels();
    $options = $this->options;
    $result = array_reduce($keys, function ($carry, $key) use ($view_fields_labels, $options) {
      if (isset($view_fields_labels[$options[$key . '_field']])) {
        $carry['mapped'][] = $key;
      }
      else {
        $carry['unmapped'][] = $key;
      }
      return $carry;
    }, ['mapped' => [], 'unmapped' => []]);

    $podcast_elements = array_merge(
      array_map([$this, 'buildElementFromOptions'], $result['mapped']),
      $podcast_elements
    );
    foreach ($result['unmapped'] as $key) {
      if ($this->options[$key . '_field']) {
        $podcast_elements[] = [
          'key' => $key,
          'value' => $this->options[$key . '_field'],
        ];
      }
    }

    $owner_name = $this->getField(0, $this->options['itunes:owner--name_field']);
    $owner_email = $this->getField(0, $this->options['itunes:owner--email_field']);
    if (!empty($owner_email) || !empty($owner_name)) {
      $podcast_elements[] = [
        'key' => 'itunes:owner',
        'values' => [
          ['key' => 'itunes:name', 'value' => $owner_name],
          ['key' => 'itunes:email', 'value' => $owner_email],
        ],
      ];
    }
    $is_locked = $this->getField(0, $this->options['podcast:locked_field']);
    if (!is_null($is_locked)) {
      $podcast_elements[] = [
        'key' => 'podcast:locked',
        'value' => strtolower($is_locked),
        'attributes' => [
          'owner' => $owner_email,
        ],
      ];
    }
    $link_keys = ['link', 'itunes:new-feed-url'];
    $podcast_elements = array_reduce($link_keys, function ($elements, $key) {
      return array_merge($elements, [$this->buildElementForLink($key)]);
    }, $podcast_elements);
    $categories = $this->buildElementFromOptions('itunes:category');
    if (!empty($categories)) {
      $category_elements = $this->processCategories($categories);
      $podcast_elements = array_merge($podcast_elements, $category_elements);
    }
    return $podcast_elements;
  }

  /**
   * Processes categories to output the format expected by iTunes.
   *
   * @param array $element
   *   The keyvalue to process.
   *
   * @return array
   *   An array of keyvalue elements representing podcast categories.
   */
  protected function processCategories(array $element): array {
    $tag_name = 'itunes:category';
    /** @var string[] $values */
    $values = array_map('trim', explode(',', $element['value']));
    // We need to parse out an optional leading category.
    $hierarchical_categories = array_reduce($values, function ($carry, $value) {
      $parts = explode('/', $value);
      if (empty($parts)) {
        return $carry;
      }
      $category = array_shift($parts);
      // Initialize the category section if it does not exist.
      if (!array_key_exists($category, $carry)) {
        $carry[$category] = [];
      }
      if (empty($parts)) {
        return $carry;
      }
      $carry[$category][] = array_shift($parts);
      return $carry;
    }, []);
    if (empty($hierarchical_categories)) {
      return [];
    }
    $new_elements = [];
    foreach ($hierarchical_categories as $category => $subcategories) {
      $category_container = $this->buildElementFromOptions('itunes:category');
      $category_container['attributes'] = ['text' => html_entity_decode($category)];
      $category_container['value'] = Markup::create(
        implode(
          array_map(function ($subcategory) use ($tag_name) {
            return sprintf('<%s text="%s"/>', $tag_name, $subcategory);
          }, $subcategories)
        )
      );
      $new_elements[] = $category_container;
    }
    return $new_elements;
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();
    $this->podcastElements = array_merge(
      $this->getPodcastElements(),
      $this->podcastElements ?? []
    );
    return $build;
  }

}
