<?php

namespace Drupal\podcast\Plugin\views\row;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\podcast\PodcastViewsMappingsTrait;
use Drupal\views\Plugin\views\row\RssFields as ViewsRssFields;

/**
 * Renders an RSS item based on fields.
 *
 * @ViewsRow(
 *   id = "podcast_rss_fields",
 *   title = @Translation("Podcast Fields"),
 *   help = @Translation("Display fields as podcast RSS items."),
 *   theme = "views_view_row_rss_podcast_feed",
 *   display_types = {"feed"}
 * )
 */
class RssFields extends ViewsRssFields {

  use PodcastViewsMappingsTrait;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['enclosure_field_options']['contains']['enclosure_field_url'] = ['default' => ''];
    $options['enclosure_field_options']['contains']['enclosure_field_length'] = ['default' => ''];
    $options['enclosure_field_options']['contains']['enclosure_field_type'] = ['default' => ''];
    $options['itunes:author_field'] = ['default' => ''];
    $options['itunes:keywords_field'] = ['default' => ''];
    $options['itunes:explicit_field'] = ['default' => ''];
    $options['itunes:duration_field'] = ['default' => ''];
    $options['itunes:summary_field'] = ['default' => ''];
    $options['itunes:image_field'] = ['default' => ''];
    $options['itunes:season_field'] = ['default' => ''];
    $options['itunes:episode_field'] = ['default' => ''];
    $options['podcast:chapters_field'] = ['default' => ''];
    $options['podcast:transcript_field'] = ['default' => ''];
    $options['podcast:soundbite_start_field'] = ['default' => ''];
    $options['podcast:soundbite_duration_field'] = ['default' => ''];
    $options['podcast:soundbite_description_field'] = ['default' => ''];
    $options['podcast:person_field'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    parent::buildOptionsForm($form, $form_state);

    $initial_labels = ['' => $this->t('- None -')];
    $view_fields_labels = $this->displayHandler->getFieldLabels();
    $view_fields_labels = array_merge($initial_labels, $view_fields_labels);

    $form['link_field']['#description'] .= ' ' . $this->t('URL must contain a leading slash. Example: /foo/bar/baz.');
    $form['enclosure_field_options'] = [
      '#type' => 'details',
      '#title' => $this->t('Audio file settings'),
      '#open' => TRUE,
    ];
    $form['enclosure_field_options']['enclosure_field_url'] = [
      '#type' => 'select',
      '#title' => $this->t('URL field'),
      '#description' => $this->t('The globally unique identifier of the RSS item.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['enclosure_field_options']['enclosure_field_url'],
      '#required' => TRUE,
    ];
    $form['enclosure_field_options']['enclosure_field_length'] = [
      '#type' => 'select',
      '#title' => $this->t('Length field'),
      '#description' => $this->t('The length of the episode.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['enclosure_field_options']['enclosure_field_length'],
    ];
    $form['enclosure_field_options']['enclosure_field_type'] = [
      '#type' => 'select',
      '#title' => $this->t('MIME type field'),
      '#description' => $this->t('The MIME for the audio file.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['enclosure_field_options']['enclosure_field_type'],
    ];
    $form['itunes:author_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Author field'),
      '#description' => $this->t('Authors of the podcast for iTunes meta.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:author_field'],
    ];
    $form['itunes:keywords_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Keywords field'),
      '#description' => $this->t('Keywords to display in iTunes.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:keywords_field'],
    ];
    $form['itunes:explicit_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Explicit field'),
      '#description' => $this->t('Mark the episode as being explicit or not. Format: "true" / "false".'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:explicit_field'],
    ];
    $form['itunes:duration_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Duration field'),
      '#description' => $this->t('iTunes Episode duration'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:duration_field'],
    ];
    $form['itunes:summary_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Summary field'),
      '#description' => $this->t('A brief summary of the episode to display in iTunes.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:summary_field'],
    ];
    $form['itunes:image_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Image field'),
      '#description' => $this->t('An image for the episode to display in iTunes.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:image_field'],
    ];
    $form['itunes:season_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Season field'),
      '#description' => $this->t('A season number for display in iTunes.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:season_field'],
    ];
    $form['itunes:episode_field'] = [
      '#type' => 'select',
      '#title' => $this->t('iTunes Episode field'),
      '#description' => $this->t('An episode number for display in iTunes.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['itunes:episode_field'],
    ];
    $form['podcast:chapters_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Chapters'),
      '#description' => $this->t('Links to an external file containing chapter data for the episode'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:chapters_field'],
    ];
    $form['podcast:transcript_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Transcript'),
      '#description' => $this->t('Links to file containing transcript or closed captions for the episode. Currently only supports one file. Supported formats: text/plain, text/html, text/vtt, application/json, application/x-subrip'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:transcript_field'],
    ];
    $form['podcast:soundbite_start_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Soundbite Start Time'),
      '#description' => $this->t('The time where the soundbite begins in seconds.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:soundbite_start_field'],
    ];
    $form['podcast:soundbite_duration_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Soundbite Duration'),
      '#description' => $this->t('How long is the soundbite (recommended between 15 and 120 seconds).'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:soundbite_duration_field'],
    ];
    $form['podcast:soundbite_description_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Soundbite Description'),
      '#description' => $this->t('Please do not exceed 128 characters for the node value or it may be truncated by aggregators.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:soundbite_description_field'],
    ];
    $form['podcast:person_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Person'),
      '#description' => $this->t('This element specifies a person of interest to the podcast. It is primarily intended to identify people like hosts, co-hosts and guests. For multiple make them comma separated.'),
      '#options' => $view_fields_labels,
      '#default_value' => $this->options['podcast:person_field'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($row): array {
    static $row_index;
    if (!isset($row_index)) {
      $row_index = 0;
    }
    $build = parent::render($row);
    $item = $build['#row'];
    $description = $this->buildElementFromOptions('description', $row_index);
    $item->description = NestedArray::getValue($description, ['value']);

    $keyval_url = $this->buildElementForLink(NULL, $row_index, [
      'enclosure_field_options',
      'enclosure_field_url',
    ]);
    $keyval_length = $this->buildElementFromOptions(NULL, $row_index, [
      'enclosure_field_options',
      'enclosure_field_length',
    ]);
    $keyval_type = $this->buildElementFromOptions(NULL, $row_index, [
      'enclosure_field_options',
      'enclosure_field_type',
    ]);
    $image_href = $this->buildElementFromOptions(NULL, $row_index, [
      'itunes:image_field',
    ]);
    if ($image_href) {
      $item->elements[] = [
        'key' => 'itunes:image',
        'attributes' => [
          'href' => $image_href,
        ],
      ];
    }
    $item->elements[] = [
      'key' => 'enclosure',
      'attributes' => [
        'url' => NestedArray::getValue($keyval_url, ['value']),
        'length' => NestedArray::getValue($keyval_length, ['value']),
        'type' => NestedArray::getValue($keyval_type, ['value']),
      ],
    ];
    $item->elements[] = $this->buildElementFromOptions('itunes:title', $row_index, [
      'title_field',
    ]);
    $link_keys = ['link'];
    $item->elements = array_reduce(
      $link_keys,
      function ($elements, $key) use ($row_index) {
        return array_merge(
          $elements,
          [$this->buildElementForLink($key, $row_index)]
        );
      },
      $item->elements
    );
    $chapters_url = $this->buildElementForLink(NULL, $row_index, [
      'podcast:chapters_field',
    ]);
    $item->elements[] = [
      'key' => 'podcast:chapters',
      'attributes' => [
        'url' => NestedArray::getValue($chapters_url, ['value']),
        'type' => 'application/json+chapters',
      ],
    ];
    $transcript_url = $this->buildElementForLink(NULL, $row_index, [
      'podcast:transcript_field',
    ]);
    if (!empty($transcript_url)) {
      $transcript_url_path = NestedArray::getValue($transcript_url, ['value']);
      $transcript_file_type = pathinfo($transcript_url_path, PATHINFO_EXTENSION);
      switch ($transcript_file_type) {
        case 'html':
          $transcript_type = 'text/html';
          break;

        case 'vtt':
          $transcript_type = 'text/vtt';
          break;

        case 'json':
          $transcript_type = 'application/json';
          break;

        case 'srt':
          $transcript_type = 'application/x-subrip';
          break;

        default:
          $transcript_type = 'text/plain';

      }
      $item->elements[] = [
        'key' => 'podcast:transcript',
        'attributes' => [
          'url' => NestedArray::getValue($transcript_url, ['value']),
          'type' => $transcript_type,
        ],
      ];
    }
    $item->elements[] = [
      'key' => 'podcast:season',
      'value' => $this->buildElementFromOptions('itunes:season', $row_index)['value'],
    ];
    $item->elements[] = [
      'key' => 'podcast:episode',
      'value' => $this->buildElementFromOptions('itunes:episode', $row_index)['value'],
    ];

    $soundbite_start = $this->buildElementFromOptions('podcast:soundbite_start', $row_index)['value'];
    if (!is_null($soundbite_start)) {
      $item->elements[] = [
        'key' => 'podcast:soundbite',
        'value' => $this->buildElementFromOptions('podcast:soundbite_description', $row_index)['value'],
        'attributes' => [
          'startTime' => $soundbite_start,
          'duration' => $this->buildElementFromOptions('podcast:soundbite_duration', $row_index)['value'],
        ],
      ];
    }
    $person = $this->buildElementFromOptions('podcast:person', $row_index)['value'];
    if (!is_null($person)) {
      $people = explode(',', $person);
      foreach ($people as $ind_person) {
        $item->elements[] = [
          'key' => 'podcast:person',
          'value' => $ind_person,
        ];
      }
    }
    $keys = [
      'description',
      'itunes:author',
      'itunes:keywords',
      'itunes:explicit',
      'itunes:duration',
      'itunes:summary',
      'itunes:season',
      'itunes:episode',
    ];
    $item->elements = array_reduce(
      $keys,
      function ($elements, $key) use ($row_index) {
        return array_merge(
          $elements,
          [$this->buildElementFromOptions($key, $row_index)]
        );
      },
      $item->elements
    );
    $item->elements = array_filter($item->elements);
    $build['#row'] = $item;

    $row_index++;
    return $build;
  }

}
