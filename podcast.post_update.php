<?php

/**
 * @file
 * Post update hooks for the podcast module.
 */

use Drupal\views\Views;

/**
 * Reconfigures views to use select or other on the copyright field.
 */
function podcast_post_update_copyright_field() {
  \Drupal::service('module_installer')->install(['select_or_other']);
  $style_plugin_id = 'podcast_rss';
  $all_views = Views::getAllViews();
  foreach ($all_views as $view) {
    $displays = $view->get('display');
    $changed = FALSE;
    foreach ($displays as &$display) {
      if ($display['display_plugin'] === 'feed' &&
        isset($display['display_options']['style']['type']) &&
        $display['display_options']['style']['type'] === $style_plugin_id &&
        isset($style_options['copyright']) &&
        !isset($style_options['copyright_field'])) {
        // Rename copyright to copyright_field.
        $style_options = $display['display_options']['style']['options'];
        $style_options['copyright_field'] = $style_options['copyright'];
        unset($style_options['copyright']);
        $display['display_options']['style']['options'] = $style_options;
        $changed = TRUE;
      }
    }
    if ($changed) {
      $view->set('display', $displays);
      $view->save();
    }
  }
}
